<?php
session_start();
if(! isset($_SESSION['loggedin']))
    exit('access forbidden');

include('config.php');

$judul = "Form Edit Artikel";
$subjudul = "";
$banner_url = "assets/img/home-bg.jpg";

$id = $_GET['id'];

$query = mysqli_query($con, 'SELECT * FROM posts WHERE id = '. $id);
$post = mysqli_fetch_assoc($query);

if(isset($_POST['submit'])){
    $judul = $_POST['judul'];
    $konten = $_POST['konten'];
    // $date_created = date("Y-m-d H:i:s");

    $query = mysqli_query($con, 'UPDATE posts SET judul = "'.$judul.'", konten = "'.$konten.'" WHERE id = '. $id);

    if($query){
        header('Location: index.php');
    } else {
        $error = "Error: " . mysqli_error($con);
    }
}
?>

<?php include('header.php'); ?>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <?php if(isset($error)): ?>
                <div class="alert alert-warning"><?php echo $error; ?></div>
            <?php endif;?>
        
            <form action="edit_post.php?id=<?php echo $id; ?>" method="post">
                <label>Judul Artikel</label>
                <input type="text" name="judul" value="<?php echo $post['judul']; ?>" class="form-control">
                <br>
                <label>Konten Artikel</label>
                <textarea name="konten" class="form-control"><?php echo $post['konten']; ?></textarea>

                <br>
                <button type="submit" name="submit" value="submit">Submit Artikel</button>
            </form>

        </div>
    </div>

    <hr>

<?php include('footer.php'); ?>

