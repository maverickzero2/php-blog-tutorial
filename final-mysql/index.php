<?php
include('config.php');

$data = mysqli_query($con, 'SELECT * FROM posts WHERE deleted = 0 ORDER BY date_created DESC');
$posts = mysqli_fetch_all($data, MYSQLI_ASSOC);

$judul = "Welcome";
$subjudul = "Ini Blog Saya";
$banner_url = "assets/img/home-bg.jpg";
?>

<?php include('header.php'); ?>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
        
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <?php foreach($posts as $post): ?>
                <div class="post-preview">
                    <a href="post.php?id=<?php echo $post['id']; ?>">
                        <h2 class="post-title">
                            <?php echo $post['judul']; ?>
                        </h2>
                    </a>
                    <p><?php if(empty($post['intro'])){
                        echo substr($post['konten'], 0, 200) . '...';
                        } else {
                        echo $post['intro'];
                            }; ?></p>
                    <p class="post-meta">Posted on <?php echo date("d F Y", strtotime($post['date_created'])); ?></p>
                </div>
                <hr>
                <?php endforeach; ?>
                
                <!-- Pager -->
                <ul class="pager">
                    <li class="next">
                        <a href="#">Older Posts &rarr;</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <hr>

<?php include('footer.php'); ?>