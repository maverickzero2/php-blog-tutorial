<?php
session_start();
if(! isset($_SESSION['loggedin']))
    exit('access forbidden');

include('config.php');

$id = $_GET['id'];

$query = mysqli_query($con, 'UPDATE posts SET deleted = 1 WHERE id = '. $id);

if($query){
	header('Location: index.php');
} else {
	exit('Deleting Failed: '. mysqli_error($con));
}