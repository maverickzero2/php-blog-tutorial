<?php
session_start();
include('functions.php');

if(! isset($_SESSION['loggedin']))
    exit('access forbidden');

$id = $_GET['id'];
$file = $post_folder.'/'.$id.'.json';
if(!file_exists($file))
    die('post tidak ditemukan.');

$post = get_post_content($file);

if(isset($_POST['submit'])){
    $data['title'] = $_POST['title'];
    $data['content'] = $_POST['content'];

    if($result = write_post($id, $data)){
        header('Location: edit_post.php?id='.$id);
    } else {
        $error = "Error: " . $result;
    }
}

// deskripsi halaman
$judul = "Form Edit Artikel";
$subjudul = "";
$banner_url = "assets/img/home-bg.jpg";
?>

<?php include('shared/header.php'); ?>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <?php if(isset($error)): ?>
                <div class="alert alert-warning"><?php echo $error; ?></div>
            <?php endif;?>
        
            <form action="edit_post.php?id=<?php echo $id; ?>" method="post">
                <label>Judul Artikel</label>
                <input type="text" name="title" value="<?php echo $post['title']; ?>" class="form-control">
                <br>
                <label>Konten Artikel</label>
                <textarea name="content" class="form-control" rows="20"><?php echo $post['content']; ?></textarea>

                <br>
                <button type="submit" name="submit" value="submit">Submit Artikel</button>
            </form>

        </div>
    </div>

    <hr>

<?php include('shared/footer.php'); ?>

