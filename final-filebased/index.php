<?php
session_start();
include('functions.php');

$judul = "Welcome";
$subjudul = "Ini Blog Saya";
$banner_url = "assets/img/home-bg.jpg";

$posts = get_posts();
?>

<?php include('shared/header.php'); ?>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
        
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
            
                <?php foreach($posts as $post): ?>
                <div class="post-preview">
                    <a href="post.php?id=<?php echo $post['slug']; ?>">
                        <h2 class="post-title">
                            <?php echo $post['title']; ?>
                        </h2>
                    </a>
                    <p><?php if(empty($post['intro'])){
                        echo substr($post['content'], 0, 200) . '...';
                        } else {
                        echo $post['intro'];
                            }; ?></p>
                    <p class="post-meta">Posted on <?php echo date("d F Y", strtotime($post['date_created'])); ?> 

                        <?php if(isset($_SESSION['loggedin'])): ?>
                        &bull; <a href="edit_post.php?id=<?php echo $post['slug']; ?>">edit</a> 
                        &bull; <a href="delete_post.php?id=<?php echo $post['slug']; ?>">delete</a>
                        <?php endif; ?>
                    </p>
                </div>
                <hr>
                <?php endforeach; ?>
                
                <!-- Pager -->
                <!-- <ul class="pager">
                    <li class="next">
                        <a href="#">Older Posts &rarr;</a>
                    </li>
                </ul> -->
            </div>
        </div>
    </div>

    <hr>

<?php include('shared/footer.php'); ?>