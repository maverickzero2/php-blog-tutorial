<?php
session_start();
include('functions.php');

if(! isset($_SESSION['loggedin']))
    exit('access forbidden');

$id = $_GET['id'];
$file = $post_folder.'/'.$id.'.json';
if(!file_exists($file))
    die('post tidak ditemukan.');

if(unlink($file)){
	header('Location: index.php');
} else {
	exit('Deleting Failed');
}