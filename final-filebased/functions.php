<?php

$post_folder = 'posts';

function get_posts()
{
	// get global var
	global $post_folder;

	// get post file list
	chdir($post_folder);
	$files = glob('*.json');

	// parse post content
	$posts = [];
	foreach ($files as $file) {
		$posts[$file] = get_post_content($file);
	}

	return $posts;
}

function get_post_content($filename)
{
	global $post_folder;

	$file = $filename;
	if(!file_exists($file)) return false;

	$post = file_get_contents($file);
	$post = json_decode($post, true);
	$fileinfo = pathinfo($filename);
	$post['slug'] = $fileinfo['filename'];
	list($date_created, $slugtitle) = explode("-", $post['slug'], 2);
	$post['date_created'] = date("Y-m-d H:i:s", $date_created);

	return $post;
}

function write_post($filename, $data)
{
	global $post_folder;

	$content = json_encode($data);
	if(file_put_contents($post_folder.'/'.$filename.'.json', $content))
		return true;

	return false;
}